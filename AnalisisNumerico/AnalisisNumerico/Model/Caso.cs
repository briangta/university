﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Model
{
    public class Caso
    {
        float x;
        string h;
        string funcion;

        public float derivadaRemplazada { get; set; }

        public Caso(float x, string h, string funcion)
        {
            this.x = x;
            this.h = h;
            this.funcion = funcion;
        }

        public string calcularDerivada()
        {
            string funcionRemplazada = funcion.Replace("X", x.ToString());
            string[] operacionesVariables = funcion.Split(' ');
            string derivadaTotal = string.Empty;
            float resultado = 0;
            int count = 0;
            string derivadaTemp = string.Empty;
            bool suma = true;

            foreach (string a in operacionesVariables)
            {
                if (!string.IsNullOrWhiteSpace(a))
                {
                    count++;
                    if (a.Contains("-"))
                    {
                        suma = false;
                        derivadaTemp = a.Replace("-", "");
                    }
                    else
                    {
                        suma = true;
                        derivadaTemp = a.Replace("+", "");
                    }

                    float resultadoTemp = 0;

                    if (a.Contains("^"))
                    {
                        float numero = !string.IsNullOrWhiteSpace(derivadaTemp.Substring(0, derivadaTemp.IndexOf("X"))) ? float.Parse(derivadaTemp.Substring(0, derivadaTemp.IndexOf("X"))) : 1;
                        float exponente = !string.IsNullOrWhiteSpace(derivadaTemp.Substring(derivadaTemp.IndexOf("^") + 1)) ? float.Parse(derivadaTemp.Substring(derivadaTemp.IndexOf("^") + 1)) : 1;
                        derivadaTemp = (numero * exponente).ToString();


                        if (exponente > 1)
                        {
                            derivadaTemp += "X^" + (exponente - 1);
                            resultadoTemp = (float)((numero * exponente) * Math.Pow(x, exponente - 1));
                        }
                        else
                        {
                            resultadoTemp = numero;
                        }



                    }

                    if (a.Contains("Sen"))
                    {
                        derivadaTemp = " Cos x ";
                        resultadoTemp = (float)Math.Cos(x);
                    }

                    if (a.Contains("Cos"))
                    {
                        derivadaTemp = "(-sin x)";
                        resultadoTemp = (float)-Math.Sin(x);
                    }

                    resultado += suma ? resultadoTemp : -resultadoTemp;

                    if (count - 1 > 0)
                    {
                        if (suma)
                        {
                            derivadaTotal += "+" + derivadaTemp;
                        }
                        else
                        {
                            derivadaTotal += "-" + derivadaTemp;
                        }
                    }
                    else
                    {
                        derivadaTotal += derivadaTemp;
                    }
                }
            }
            //return string.Join(" ", operacionesVariables);
            derivadaRemplazada = resultado;
            return derivadaTotal + "~" + resultado;
        }

        string Formula1(float h)
        {
            float resultadoFormula = ((derivadaRemplazada + h) - derivadaRemplazada) / h;
            float error = ((resultadoFormula - x) / resultadoFormula)*100;
            return resultadoFormula + "~" + error;
        }

        string Formula2(float h)
        {
            float resultadoFormula = (derivadaRemplazada - (derivadaRemplazada - h)) / h;
            float error = ((resultadoFormula - x) / resultadoFormula) * 100;
            return resultadoFormula + "~" + error;
        }

        string Formula3(float h)
        {
            float resultadoFormula = ((derivadaRemplazada + h) - (derivadaRemplazada - h)) / h;
            float error = ((resultadoFormula - x) / resultadoFormula) * 100;
            return resultadoFormula + "~" + error;
        }

        public string[] calcularFormulas()
        {
            string[] resultadosDividido = { "", "", "" };

            foreach(string a in h.Split('~'))
            {
                float m = float.Parse(a);
                resultadosDividido[0] += Formula1(m) + " ";
                resultadosDividido[1] += Formula2(m) + " ";
                resultadosDividido[2] += Formula3(m) + " ";
            }

            return resultadosDividido;
        }
    }
}
