﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Model
{
    public class EulerMejorado
    {
        int n;
        float h,x,y;

        public EulerMejorado(float x, float y, int n,float h)
        {
            this.x = x;
            this.y = y;
            this.n = n;
            this.h = h;

            /*h = (x + 1 - x) / n;*/

            xList.Add(x);
            yList.Add(y);
            //uList.Add(calcularU());
        }

        List<float> xList = new List<float>();
        List<float> yList = new List<float>();
        List<float> uList = new List<float>();

        public float calcularU() {
            float resultado = yList.Last() + h * reemplazarFunction(xList.Last(), yList.Last());
            return resultado;
        }

        public float calcularX()
        {
            float result = xList.Last() + h;
           // xList.Add(result);
            return result;
        }

        public float calcularY()
        {
            float penultimoX = xList.TakeWhile(r=>r!=xList.Last()).LastOrDefault();
            //float penultimoY = yList.TakeWhile(r => r != uList.Last()).LastOrDefault();

            float result = yList.Last() + h*(0.5f)
                *(reemplazarFunction(penultimoX, yList.Last())+ reemplazarFunction(xList.Last(), uList.Last()));

            return result;
        }

        public float reemplazarFunction(float x,float y)
        {

            return (float)Math.Sqrt(y) / (2 * x + 1);

        }

        public float calcularResultado()
        {
            for (int q = 1; q <= n; q++)
            {
                float x = calcularX();
                xList.Add(x);
                float u = calcularU();
                uList.Add(u);
                float y = calcularY();
                yList.Add(y);
            }

            return yList.Last();
        }


    }

}
