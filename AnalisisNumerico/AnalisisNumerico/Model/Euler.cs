﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Model
{
    public class Euler
    {
        int n;
        float h,x,y;

        public Euler(float x, float y, int n,float h)
        {
            this.x = x;
            this.y = y;
            this.n = n;
            this.h = h;

            /*h = (x + 1 - x) / n;*/

            xList.Add(x);
            yList.Add(y);
        }

        List<float> xList = new List<float>();
        List<float> yList = new List<float>();

        public float calcularX()
        {
            float result = xList.Last() + h;
           // xList.Add(result);
            return result;
        }

        public float calcularY()
        {
            float result = yList.Last() + h*reemplazarFunction(xList.Last(),yList.Last());
            return result;
        }

        public float reemplazarFunction(float x,float y)
        {

            return (float)Math.Sqrt(y) / (2 * x + 1);

        }

        public float calcularResultado()
        {
            for(int q = 1; q <= n; q++)
            {
                float x = calcularX();
                float y = calcularY();
                xList.Add(x);
                yList.Add(y);
            }

            return yList.Last();
        }


    }

}
