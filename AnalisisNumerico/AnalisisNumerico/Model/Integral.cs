﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Model
{
    class Integral
    {
        int a, b, n;
        float deltaX;
        List<float> resultadosPrimerPaso = new List<float>();
        List<float> resultadosSegundoPaso = new List<float>();


        public Integral(int a, int b, int n)
        {
            this.a = a;
            this.b = b;
            this.n = n;            
            deltaX= ((b - a) / n);
        }

        private float calcularPrimerPaso(int i)
        {
            float xi = a + i * deltaX;
            resultadosPrimerPaso.Add(xi);
            return xi;
        }

        private void calcularSegundoPaso(float xi)
        {
            float fxi = 3*(float)Math.Pow(xi,2);
            resultadosSegundoPaso.Add(fxi);
        }

        public float calcularTercerPaso()
        {
            for(int q = 0; q <= n; q++)
            {
                calcularSegundoPaso(calcularPrimerPaso(q));
            }

            float resultado = 0;
            float primero = resultadosSegundoPaso.First()
                ,ultimo = resultadosSegundoPaso.Last();

            resultadosSegundoPaso.Remove(primero);
            resultadosSegundoPaso.Remove(ultimo);

            resultado += primero + ultimo;

            resultado += resultadosSegundoPaso.Select(r => r * 2).Sum();

            resultado = (deltaX / 2) * resultado;

            return resultado;
        }


    }
}
