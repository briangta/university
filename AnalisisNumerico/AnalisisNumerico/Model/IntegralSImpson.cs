﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Model
{
    class IntegralSimpson
    {
        float a, b;
        int n2;
        float deltaX;
        List<float> primerPaso,segundoPaso;

        public IntegralSimpson(int n2, float a, float b)
        {
            this.n2 = n2;
            this.a = a;
            this.b = b;

            primerPaso = new List<float>();
            segundoPaso = new List<float>();
        }


        private float calcularPrimerPaso()
        {
            float valor = primerPaso.Last()+deltaX;
            primerPaso.Add(valor);
            return valor;
        }

        private void calcularSegundoPaso(float valor)
        {
            float resultado = 3 * (float)Math.Pow(valor, 2);
            segundoPaso.Add(resultado);
        }

        public float calcularIntegral()
        {
            deltaX = (b - a) / n2;

            primerPaso.Add(0);
            calcularSegundoPaso(0);

            for(int q = 0; q <= n2; q++)
            {
                calcularSegundoPaso(calcularPrimerPaso());
            }

            float resultado = (segundoPaso.First() + segundoPaso.Last());

            segundoPaso.Remove(segundoPaso.First());
            segundoPaso.Remove(segundoPaso.Last());

            bool multiplicador4 = true;
            foreach(float num in segundoPaso)
            {
                if (multiplicador4)
                {
                    resultado += num * 4;
                    multiplicador4 = false;
                }
                else
                {
                    resultado += num * 2;
                    multiplicador4 = true;
                }
            }

            return resultado;
        }


    }
}
