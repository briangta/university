﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalisisNumerico
{
    public partial class Taller2Corte : Form
    {
        public Taller2Corte()
        {
            InitializeComponent();
        }

        private void Taller2Corte_Load(object sender, EventArgs e)
        {
            String[] funciones = { "SenX", "CosX", "mX^n" };
            String[] operaciones = { "", "+","-" };
            FuncionesSelect.DataSource = funciones;
            OperadoresSelect.DataSource = operaciones;
        }

        private void AgregarFuncion_Click(object sender, EventArgs e)
        {
            int n = (int)nTextBox.Value, m = (int)mTextBox.Value;

            string funcion = FuncionesSelect.SelectedItem.ToString()+" " + OperadoresSelect.SelectedItem.ToString();

            funcion = funcion.Replace("^n", "^" + nTextBox.Value.ToString());


            if (m < 2)
            {
                funcion = funcion.Replace("m", "");
            }
            else
            {
                funcion = funcion.Replace("m", mTextBox.Value.ToString());
            }

            FuncionTexto.Text += funcion;
        }

        private void Limpiar_Click(object sender, EventArgs e)
        {
            FuncionTexto.Text = string.Empty;
        }

        private void DerivarButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FuncionTexto.Text) && !string.IsNullOrEmpty(hTextBox.Text))
            {
                Model.Caso caso = new Model.Caso((float)xTextBox.Value, hTextBox.Text, FuncionTexto.Text);
                string[] resultado = caso.calcularDerivada().Split('~');
                Derivada.Text = "f'(x)="+resultado[0];
                DerivadaResueltaTextBox.Text= "f'("+ xTextBox.Value+ ")=" + resultado[1];

                string[] resultadoFormulas=caso.calcularFormulas();

                Formula1.Text = "Formula 1 =" + resultadoFormulas[0];
                Formula2.Text = "Formula 2 =" + resultadoFormulas[1];
                Formula3.Text = "Formula 3 =" + resultadoFormulas[2];

            }
            else
            {
                MessageBox.Show("No puede estar vacio f(x) ni h");
            }
        }

        private void AgregarH_Click(object sender, EventArgs e)
        {
            string hText = hTextBox.Text;
            if (!string.IsNullOrWhiteSpace(hText))
                hText += "~";
            hText += hInput.Value.ToString();
            hTextBox.Text = hText;
        }
    }
}
