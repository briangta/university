﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EcuacionCuadratica
{
    public partial class SerieTaylor : Form
    {
        public SerieTaylor()
        {
            InitializeComponent();
        }


        static double calcucos(double angulo, double cantidad)
        {
            double resultado = 0, elemento = 0;
            int po;
            for (int i = 0; i <= cantidad; i++)
            {
                po = i * 2;
                if (i % 2 == 0)
                {
                    elemento = Math.Pow(angulo, po) / factorial(po);
                }
                else
                {
                    elemento = -(Math.Pow(angulo, po) / factorial(po));
                }
                resultado += elemento;
                if (Math.Abs(elemento) < 0.0005) break;
            }
            return resultado;
        }

        static double factorial(int m)
        {
            if (m == 0)
            {
                return 1;
            }
            else
            {
                int factorial = 1;
                for (int i = m; i >= 1; i--)
                {
                    factorial = factorial * i;
                }
                return factorial;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double anggrad, angrad;
            double coseno, tserie;
            tserie = (double)numericUpDown1.Value;
            Console.WriteLine("Ingrese el Angulo en Grados");
            anggrad = (double)numericUpDown2.Value;
            angrad = (anggrad * Math.PI) / 180;
            coseno = calcucos(angrad, tserie);
            resultado.Text = coseno.ToString();
        }
    }
}
