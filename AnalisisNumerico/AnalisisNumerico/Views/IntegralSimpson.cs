﻿using System;
using System.Windows.Forms;

namespace AnalisisNumerico
{
    public partial class IntegralSimpson : Form
    {
        public IntegralSimpson()
        {
            InitializeComponent();
        }

        private void CalcularButton_Click(object sender, EventArgs e)
        {
            Model.IntegralSimpson integralSimpson = new Model
                .IntegralSimpson((int)nTextBox.Value, (float)aTextBox.Value, (float)bTextBox.Value);

            float resultado=integralSimpson.calcularIntegral();

            ResultadoTextBox.Text = "Resultado= " + resultado;
        }
    }
}
