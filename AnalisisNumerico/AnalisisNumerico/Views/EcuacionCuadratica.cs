﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace EcuacionCuadratica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a = 0, b = 0, c = 0;
            try
            {
                a = int.Parse(textBox1.Text);
                b = int.Parse(textBox2.Text);
                c = int.Parse(textBox3.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Digine numeros validos");
            }
            Complex raiz = Complex.Sqrt(Math.Pow(b, 2) - 4 * a * c);
            if (raiz.Imaginary > 0)
            {
                textBox4.Text = "(-" + b + "+ i" + Math.Round(raiz.Imaginary, 2) + ")/" + (2 * a);
                textBox5.Text = "(-" + b + "- i" + Math.Round(raiz.Imaginary, 2) + ")/" + (2 * a);
            }
            else
            {
                textBox4.Text = Math.Round(((-b + raiz.Magnitude) / (2 * a)),2).ToString();
                textBox5.Text = Math.Round(((-b - raiz.Magnitude) / (2 * a)), 2).ToString();
            }

        }
    }
}
