﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnalisisNumerico.Model;
namespace AnalisisNumerico.Views
{
    public partial class MetodoEule : Form
    {
        public MetodoEule()
        {
            InitializeComponent();
        }

        private void calcularButton_Click(object sender, EventArgs e)
        {
            float resultadoEuler = new Euler((float)xInput.Value, (float)yInput.Value, (int)nInput.Value, (float)hInput.Value)
                .calcularResultado();

            float resultadoEulerMejorado = new EulerMejorado((float)xInput.Value, (float)yInput.Value, (int)nInput.Value, (float)hInput.Value)
               .calcularResultado();

            resultadoEulerLabel.Text = "Resultado Euler= " + resultadoEuler;
            resultadoEulerMejoradoLabel.Text = "Resultado Euler Mejorado= "+ resultadoEulerMejorado;
        }
    }
}
