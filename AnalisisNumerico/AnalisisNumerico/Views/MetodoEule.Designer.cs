﻿namespace AnalisisNumerico.Views
{
    partial class MetodoEule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xInput = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yInput = new System.Windows.Forms.NumericUpDown();
            this.calcularButton = new System.Windows.Forms.Button();
            this.nInput = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.hInput = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.resultadoEulerLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.resultadoEulerMejoradoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.xInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // xInput
            // 
            this.xInput.Location = new System.Drawing.Point(99, 82);
            this.xInput.Name = "xInput";
            this.xInput.Size = new System.Drawing.Size(45, 22);
            this.xInput.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "y(";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = ")=";
            // 
            // yInput
            // 
            this.yInput.Location = new System.Drawing.Point(173, 82);
            this.yInput.Name = "yInput";
            this.yInput.Size = new System.Drawing.Size(45, 22);
            this.yInput.TabIndex = 3;
            // 
            // calcularButton
            // 
            this.calcularButton.Location = new System.Drawing.Point(151, 204);
            this.calcularButton.Name = "calcularButton";
            this.calcularButton.Size = new System.Drawing.Size(75, 23);
            this.calcularButton.TabIndex = 4;
            this.calcularButton.Text = "Calcular";
            this.calcularButton.UseVisualStyleBackColor = true;
            this.calcularButton.Click += new System.EventHandler(this.calcularButton_Click);
            // 
            // nInput
            // 
            this.nInput.Location = new System.Drawing.Point(116, 119);
            this.nInput.Name = "nInput";
            this.nInput.Size = new System.Drawing.Size(102, 22);
            this.nInput.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "n=";
            // 
            // hInput
            // 
            this.hInput.Location = new System.Drawing.Point(116, 159);
            this.hInput.Name = "hInput";
            this.hInput.Size = new System.Drawing.Size(103, 22);
            this.hInput.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(78, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "h=";
            // 
            // resultadoEulerLabel
            // 
            this.resultadoEulerLabel.AutoSize = true;
            this.resultadoEulerLabel.Location = new System.Drawing.Point(476, 210);
            this.resultadoEulerLabel.Name = "resultadoEulerLabel";
            this.resultadoEulerLabel.Size = new System.Drawing.Size(113, 17);
            this.resultadoEulerLabel.TabIndex = 9;
            this.resultadoEulerLabel.Text = "Resultado Euler:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AnalisisNumerico.Properties.Resources.ecuacionEuler;
            this.pictureBox1.Location = new System.Drawing.Point(479, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // resultadoEulerMejoradoLabel
            // 
            this.resultadoEulerMejoradoLabel.AutoSize = true;
            this.resultadoEulerMejoradoLabel.Location = new System.Drawing.Point(413, 257);
            this.resultadoEulerMejoradoLabel.Name = "resultadoEulerMejoradoLabel";
            this.resultadoEulerMejoradoLabel.Size = new System.Drawing.Size(176, 17);
            this.resultadoEulerMejoradoLabel.TabIndex = 11;
            this.resultadoEulerMejoradoLabel.Text = "Resultado Euler Mejorado:";
            // 
            // MetodoEule
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(832, 419);
            this.Controls.Add(this.resultadoEulerMejoradoLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.resultadoEulerLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.hInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nInput);
            this.Controls.Add(this.calcularButton);
            this.Controls.Add(this.yInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.xInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MetodoEule";
            this.Text = "MetodoEule";
            ((System.ComponentModel.ISupportInitialize)(this.xInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown xInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yInput;
        private System.Windows.Forms.Button calcularButton;
        private System.Windows.Forms.NumericUpDown nInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown hInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label resultadoEulerLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label resultadoEulerMejoradoLabel;
    }
}