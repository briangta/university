﻿using AnalisisNumerico;
using System.Windows.Forms;
using AnalisisNumerico.Views;

namespace EcuacionCuadratica
{
    public partial class Main : Form
    {
        public Main()
        {

            InitializeComponent();

            Form1 form1 = new Form1();
            form1.TopLevel = false;
            form1.Parent = tabPage1;
            form1.Show();
            form1.Dock = DockStyle.Fill;
            tabPage1.Text = form1.Text;

            SerieTaylor form2 = new SerieTaylor();
            form2.TopLevel = false;
            form2.Parent = tabPage2;
            form2.Show();
            form2.Dock = DockStyle.Fill;
            tabPage2.Text = form2.Text;

            Taller2Corte form3 = new Taller2Corte();
            form3.TopLevel = false;
            form3.Parent = tabPage3;
            form3.Show();
            form3.Dock = DockStyle.Fill;

            ResolverIntegrales form4 = new ResolverIntegrales();
            form4.TopLevel = false;
            form4.Parent = tabPage4;
            form4.Show();
            form4.Dock = DockStyle.Fill;

            IntegralSimpson form5 = new IntegralSimpson();
            form5.TopLevel = false;
            form5.Parent = tabPage5;
            form5.Show();
            form5.Dock = DockStyle.Fill;

            MetodoEule form6 = new MetodoEule();
            form6.TopLevel = false;
            form6.Parent = tabPage6;
            form6.Show();
            form6.Dock = DockStyle.Fill;
        }
    }
}
