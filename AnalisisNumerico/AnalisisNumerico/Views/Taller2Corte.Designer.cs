﻿namespace AnalisisNumerico
{
    partial class Taller2Corte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FuncionesSelect = new System.Windows.Forms.ComboBox();
            this.AgregarFuncion = new System.Windows.Forms.Button();
            this.FuncionTexto = new System.Windows.Forms.TextBox();
            this.nTextBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.OperadoresSelect = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mTextBox = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.Limpiar = new System.Windows.Forms.Button();
            this.xTextBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.DerivarButton = new System.Windows.Forms.Button();
            this.Derivada = new System.Windows.Forms.Label();
            this.hInput = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.AgregarH = new System.Windows.Forms.Button();
            this.hTextBox = new System.Windows.Forms.TextBox();
            this.DerivadaResueltaTextBox = new System.Windows.Forms.Label();
            this.Formula1 = new System.Windows.Forms.Label();
            this.Formula2 = new System.Windows.Forms.Label();
            this.Formula3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hInput)).BeginInit();
            this.SuspendLayout();
            // 
            // FuncionesSelect
            // 
            this.FuncionesSelect.FormattingEnabled = true;
            this.FuncionesSelect.Location = new System.Drawing.Point(87, 68);
            this.FuncionesSelect.Name = "FuncionesSelect";
            this.FuncionesSelect.Size = new System.Drawing.Size(121, 24);
            this.FuncionesSelect.TabIndex = 0;
            // 
            // AgregarFuncion
            // 
            this.AgregarFuncion.Location = new System.Drawing.Point(181, 167);
            this.AgregarFuncion.Name = "AgregarFuncion";
            this.AgregarFuncion.Size = new System.Drawing.Size(27, 23);
            this.AgregarFuncion.TabIndex = 1;
            this.AgregarFuncion.Text = "+";
            this.AgregarFuncion.UseVisualStyleBackColor = true;
            this.AgregarFuncion.Click += new System.EventHandler(this.AgregarFuncion_Click);
            // 
            // FuncionTexto
            // 
            this.FuncionTexto.Enabled = false;
            this.FuncionTexto.Location = new System.Drawing.Point(260, 15);
            this.FuncionTexto.Name = "FuncionTexto";
            this.FuncionTexto.Size = new System.Drawing.Size(184, 22);
            this.FuncionTexto.TabIndex = 2;
            // 
            // nTextBox
            // 
            this.nTextBox.Location = new System.Drawing.Point(87, 138);
            this.nTextBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nTextBox.Name = "nTextBox";
            this.nTextBox.Size = new System.Drawing.Size(67, 22);
            this.nTextBox.TabIndex = 3;
            this.nTextBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Funcion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(218, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "f(x)=";
            // 
            // OperadoresSelect
            // 
            this.OperadoresSelect.FormattingEnabled = true;
            this.OperadoresSelect.Location = new System.Drawing.Point(87, 104);
            this.OperadoresSelect.Name = "OperadoresSelect";
            this.OperadoresSelect.Size = new System.Drawing.Size(121, 24);
            this.OperadoresSelect.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Operador";
            // 
            // mTextBox
            // 
            this.mTextBox.Location = new System.Drawing.Point(87, 167);
            this.mTextBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mTextBox.Name = "mTextBox";
            this.mTextBox.Size = new System.Drawing.Size(67, 22);
            this.mTextBox.TabIndex = 9;
            this.mTextBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(62, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "m";
            // 
            // Limpiar
            // 
            this.Limpiar.Location = new System.Drawing.Point(461, 13);
            this.Limpiar.Name = "Limpiar";
            this.Limpiar.Size = new System.Drawing.Size(75, 23);
            this.Limpiar.TabIndex = 11;
            this.Limpiar.Text = "Limpiar";
            this.Limpiar.UseVisualStyleBackColor = true;
            this.Limpiar.Click += new System.EventHandler(this.Limpiar_Click);
            // 
            // xTextBox
            // 
            this.xTextBox.DecimalPlaces = 2;
            this.xTextBox.Location = new System.Drawing.Point(445, 71);
            this.xTextBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.xTextBox.Name = "xTextBox";
            this.xTextBox.Size = new System.Drawing.Size(62, 22);
            this.xTextBox.TabIndex = 12;
            this.xTextBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(422, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "X";
            // 
            // DerivarButton
            // 
            this.DerivarButton.Location = new System.Drawing.Point(539, 70);
            this.DerivarButton.Name = "DerivarButton";
            this.DerivarButton.Size = new System.Drawing.Size(75, 23);
            this.DerivarButton.TabIndex = 14;
            this.DerivarButton.Text = "Derivar";
            this.DerivarButton.UseVisualStyleBackColor = true;
            this.DerivarButton.Click += new System.EventHandler(this.DerivarButton_Click);
            // 
            // Derivada
            // 
            this.Derivada.AutoSize = true;
            this.Derivada.Location = new System.Drawing.Point(442, 156);
            this.Derivada.Name = "Derivada";
            this.Derivada.Size = new System.Drawing.Size(77, 17);
            this.Derivada.TabIndex = 15;
            this.Derivada.Text = "Derivada= ";
            // 
            // hInput
            // 
            this.hInput.DecimalPlaces = 4;
            this.hInput.Location = new System.Drawing.Point(445, 109);
            this.hInput.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.hInput.Name = "hInput";
            this.hInput.Size = new System.Drawing.Size(74, 22);
            this.hInput.TabIndex = 16;
            this.hInput.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(422, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "h";
            // 
            // AgregarH
            // 
            this.AgregarH.Location = new System.Drawing.Point(525, 109);
            this.AgregarH.Name = "AgregarH";
            this.AgregarH.Size = new System.Drawing.Size(27, 23);
            this.AgregarH.TabIndex = 18;
            this.AgregarH.Text = "+";
            this.AgregarH.UseVisualStyleBackColor = true;
            this.AgregarH.Click += new System.EventHandler(this.AgregarH_Click);
            // 
            // hTextBox
            // 
            this.hTextBox.Location = new System.Drawing.Point(558, 110);
            this.hTextBox.Name = "hTextBox";
            this.hTextBox.Size = new System.Drawing.Size(173, 22);
            this.hTextBox.TabIndex = 19;
            // 
            // DerivadaResueltaTextBox
            // 
            this.DerivadaResueltaTextBox.AutoSize = true;
            this.DerivadaResueltaTextBox.Location = new System.Drawing.Point(480, 185);
            this.DerivadaResueltaTextBox.Name = "DerivadaResueltaTextBox";
            this.DerivadaResueltaTextBox.Size = new System.Drawing.Size(39, 17);
            this.DerivadaResueltaTextBox.TabIndex = 20;
            this.DerivadaResueltaTextBox.Text = "f\'(x)=";
            // 
            // Formula1
            // 
            this.Formula1.AutoSize = true;
            this.Formula1.Location = new System.Drawing.Point(236, 259);
            this.Formula1.Name = "Formula1";
            this.Formula1.Size = new System.Drawing.Size(83, 17);
            this.Formula1.TabIndex = 21;
            this.Formula1.Text = "Formula 1= ";
            // 
            // Formula2
            // 
            this.Formula2.AutoSize = true;
            this.Formula2.Location = new System.Drawing.Point(236, 287);
            this.Formula2.Name = "Formula2";
            this.Formula2.Size = new System.Drawing.Size(83, 17);
            this.Formula2.TabIndex = 22;
            this.Formula2.Text = "Formula 2= ";
            // 
            // Formula3
            // 
            this.Formula3.AutoSize = true;
            this.Formula3.Location = new System.Drawing.Point(236, 316);
            this.Formula3.Name = "Formula3";
            this.Formula3.Size = new System.Drawing.Size(83, 17);
            this.Formula3.TabIndex = 23;
            this.Formula3.Text = "Formula 3= ";
            // 
            // Taller2Corte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(743, 396);
            this.ControlBox = false;
            this.Controls.Add(this.Formula3);
            this.Controls.Add(this.Formula2);
            this.Controls.Add(this.Formula1);
            this.Controls.Add(this.DerivadaResueltaTextBox);
            this.Controls.Add(this.hTextBox);
            this.Controls.Add(this.AgregarH);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.hInput);
            this.Controls.Add(this.Derivada);
            this.Controls.Add(this.DerivarButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.xTextBox);
            this.Controls.Add(this.Limpiar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.mTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.OperadoresSelect);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nTextBox);
            this.Controls.Add(this.FuncionTexto);
            this.Controls.Add(this.AgregarFuncion);
            this.Controls.Add(this.FuncionesSelect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Taller2Corte";
            this.Text = "Taller2Corte";
            this.Load += new System.EventHandler(this.Taller2Corte_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox FuncionesSelect;
        private System.Windows.Forms.Button AgregarFuncion;
        private System.Windows.Forms.TextBox FuncionTexto;
        private System.Windows.Forms.NumericUpDown nTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox OperadoresSelect;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown mTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Limpiar;
        private System.Windows.Forms.NumericUpDown xTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button DerivarButton;
        private System.Windows.Forms.Label Derivada;
        private System.Windows.Forms.NumericUpDown hInput;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button AgregarH;
        private System.Windows.Forms.TextBox hTextBox;
        private System.Windows.Forms.Label DerivadaResueltaTextBox;
        private System.Windows.Forms.Label Formula1;
        private System.Windows.Forms.Label Formula2;
        private System.Windows.Forms.Label Formula3;
    }
}