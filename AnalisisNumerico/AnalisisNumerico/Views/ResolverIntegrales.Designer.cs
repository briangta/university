﻿namespace AnalisisNumerico
{
    partial class ResolverIntegrales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResolverIntegrales));
            this.aInput = new System.Windows.Forms.NumericUpDown();
            this.bInput = new System.Windows.Forms.NumericUpDown();
            this.nInput = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CalcularButton = new System.Windows.Forms.Button();
            this.resultadoInput = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // aInput
            // 
            this.aInput.Location = new System.Drawing.Point(93, 48);
            this.aInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.aInput.Name = "aInput";
            this.aInput.Size = new System.Drawing.Size(120, 22);
            this.aInput.TabIndex = 0;
            this.aInput.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bInput
            // 
            this.bInput.Location = new System.Drawing.Point(93, 91);
            this.bInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bInput.Name = "bInput";
            this.bInput.Size = new System.Drawing.Size(120, 22);
            this.bInput.TabIndex = 1;
            this.bInput.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nInput
            // 
            this.nInput.Location = new System.Drawing.Point(93, 138);
            this.nInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nInput.Name = "nInput";
            this.nInput.Size = new System.Drawing.Size(120, 22);
            this.nInput.TabIndex = 2;
            this.nInput.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "a";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "b";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "n";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AnalisisNumerico.Properties.Resources.formula;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(407, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 125);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // CalcularButton
            // 
            this.CalcularButton.Location = new System.Drawing.Point(137, 178);
            this.CalcularButton.Name = "CalcularButton";
            this.CalcularButton.Size = new System.Drawing.Size(75, 23);
            this.CalcularButton.TabIndex = 7;
            this.CalcularButton.Text = "Calcular";
            this.CalcularButton.UseVisualStyleBackColor = true;
            this.CalcularButton.Click += new System.EventHandler(this.CalcularButton_Click);
            // 
            // resultadoInput
            // 
            this.resultadoInput.AllowDrop = true;
            this.resultadoInput.AutoSize = true;
            this.resultadoInput.Location = new System.Drawing.Point(90, 219);
            this.resultadoInput.Name = "resultadoInput";
            this.resultadoInput.Size = new System.Drawing.Size(80, 17);
            this.resultadoInput.TabIndex = 8;
            this.resultadoInput.Text = "Resultado: ";
            // 
            // ResolverIntegrales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(684, 352);
            this.ControlBox = false;
            this.Controls.Add(this.resultadoInput);
            this.Controls.Add(this.CalcularButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nInput);
            this.Controls.Add(this.bInput);
            this.Controls.Add(this.aInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ResolverIntegrales";
            this.ShowInTaskbar = false;
            this.Text = "ResolverIntegrales";
            ((System.ComponentModel.ISupportInitialize)(this.aInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown aInput;
        private System.Windows.Forms.NumericUpDown bInput;
        private System.Windows.Forms.NumericUpDown nInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button CalcularButton;
        private System.Windows.Forms.Label resultadoInput;
    }
}