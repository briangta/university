﻿using System;
using System.Windows.Forms;
using AnalisisNumerico.Model;

namespace AnalisisNumerico
{
    public partial class ResolverIntegrales : Form
    {
        public ResolverIntegrales()
        {
            InitializeComponent();
        }

        private void CalcularButton_Click(object sender, EventArgs e)
        {
            Integral integral = new Integral((int)aInput.Value, (int)bInput.Value, (int)nInput.Value);
            resultadoInput.Text= "Resultado: "+integral.calcularTercerPaso();
        }
    }
}
