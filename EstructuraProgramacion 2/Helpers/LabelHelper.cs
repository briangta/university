﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstructuraProgramacion_2.Helpers
{
    public class ListaPrimosHelper
    {
        public static string ListaPrimos(int start, int end)
        {
            List<int> primos = Enumerable.Range(start + 1, end-start-1).Select(x => x).ToList();
            for(int q=start+1;q<end; q++)
            {
                for(int m = 2; m < q; m++)
                {
                    if (q % m == 0)
                    {
                        primos.Remove(q);
                        break;
                    }
                }
            }
            string tabla = "<table class='table'><thead><tr><th>Numeros Primos</th><tr></thead><tbody>";
            foreach(int a in primos)
            {
                tabla += "<tr><td>" + a + "</td></tr>";
            }

            tabla += "</tbody></table>";
            return tabla;
        }
    }
}