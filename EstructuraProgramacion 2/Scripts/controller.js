﻿var app = angular.module('App', []);
app.controller('getPeople', function ($scope, $http) {
    $http.get("/Content/personas.json").then(function (response) {
        $scope.people = response.data.people;
    });

    $scope.calcular = function () {
        var a = $scope.a, b = $scope.b;
        var impares = new Array();
        if (a === "" || b === "" || a==undefined || b== undefined)
            return false;
        a = parseInt(a);
        b = parseInt(b);
        for (var q = a + 1; q < b; q++) {
            if (q % 2 != 0) {
                impares.push(q);
            }
        }
        $scope.impares = impares;
    };

});
