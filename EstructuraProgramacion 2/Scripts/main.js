﻿$(document).ready(function () {
    $("#dro1").dropdown();

    setTimeout(function () {
        $(".hide-on-med-and-down").attr("style", "");
    }, 500);

    $('select').material_select();
    $(".button-collapse").sideNav();

    $(".nav-wrapper").append($(".menu").clone().removeClass("side-nav").addClass("right hide-on-med-and-down").removeAttr("id"));
    $("#mobile-demo").find("#dropDownMenu").remove();
    $("#contenidoDropDown").appendTo("#dropDownMenu");
    
    $("form").submit(function () {
        $("input[type='checkbox']").each(function () {
            if ($(this).is(":checked")) {
                $(this).val("true");
            } else {
                $(this).val("false");
            }
        });
    });
});