﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;

namespace EstructuraProgramacion_2.Controllers
{
    public class Taller10Controller : Controller
    {
        // GET: Taller10

        public ActionResult Index()
        {
            
            if (System.Web.HttpContext.Current.Session["People"] != null)
            {
                List<PersonaTaller10> people;
                people = (List<PersonaTaller10>)System.Web.HttpContext.Current.Session["People"];

                List<string> PersonasRepetidas = people.GroupBy(r => r.Name)
                    .Where(r => r.Count() > 1)
                    .Select(r=>r.Key )
                    .ToList();

                ViewBag.PersonasRepetidas = PersonasRepetidas;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,Name")] PersonaTaller10 personaTaller10)
        {
            if (ModelState.IsValid)
            {
                List<PersonaTaller10> people;
                if (System.Web.HttpContext.Current.Session["People"] != null)
                {
                    people = (List<PersonaTaller10>)System.Web.HttpContext.Current.Session["People"];
                }else
                {
                    people = new List<PersonaTaller10>();
                }

                people.Add(personaTaller10);
                System.Web.HttpContext.Current.Session["People"] = people;

                return RedirectToAction("Index");
            }

            return View(personaTaller10);
        }
    }
}
