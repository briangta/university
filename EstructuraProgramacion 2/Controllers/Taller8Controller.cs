using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;

namespace EstructuraProgramacion_2.Controllers
{
    public class Taller8Controller : Controller
    {
        // GET: Taller7
        public ActionResult Index(double? radio)
        {
            if (radio != null) { 
                ViewBag.radio = radio;
            }

            return View();
        }

        public ActionResult asp(double? radio)
        {
            if (radio != null)
            {
                double volumen = (4 * 3.14159265359 * Math.Pow(radio.Value, 3))/3;
                ViewBag.Volumen = volumen;
            }

            return View();
        }
    }
}