﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;
using EstructuraProgramacion_2.Models.Enumeradores;
using System.IO;

namespace EstructuraProgramacion_2.Controllers
{
    public class VirusController : Controller
    {
        private EstructuraProgramacion2Entities db = new EstructuraProgramacion2Entities();

        [Acceso(EnumRol.Admin)]
        public ActionResult Index()
        {
            var virus = db.Virus.Include(v => v.Persona);
            return View(virus.ToList());
        }

        public ActionResult CalcularVacunas()
        {
            string patronVirus = this.db.Virus
                .Where(r => r.Activo == true)
                .Select(r=>r.Patron)
                .FirstOrDefault();

            string[] patronesVirus = patronVirus.Split(',');

            List<Patrones> patrones = this.db.Patrones.ToList();

            foreach(Patrones pa in patrones)
            {
                string[] patron = pa.Patron.Split(',');
                int q = 0;
                foreach(string pat in patron)
                {
                    for(int m=q;m<patronesVirus.Length;m++)
                    {
                        if (patronesVirus[m].Contains(pat))
                        {
                            q++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                pa.VirusEliminados = q;
                if (q == patronesVirus.Length)
                    pa.VirusTotalmenteEliminado = true;

            }

            return View(patrones);
        }

        // GET: Virus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Virus virus = db.Virus.Find(id);
            if (virus == null)
            {
                return HttpNotFound();
            }
            return View(virus);
        }

        // GET: Virus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Virus/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdVirus")] Virus virus)
        {
            virus.IdPersona = Acceso.loguedUser.IdPersona;
            virus.Fecha = DateTime.Now;
            virus.Activo = true;

            HttpPostedFileBase patron = Request.Files["patronTxt"];
            virus.Patron = new StreamReader(patron.InputStream).ReadToEnd();

            ModelState.Remove("Patron");
            if (ModelState.IsValid)
            {
                db.Virus.Add(virus);
                db.SaveChanges();

                Virus virusActivo = this.db.Virus.Where(r => r.Activo == true && r.IdVirus !=virus.IdVirus).FirstOrDefault();
                if (virusActivo != null)
                {
                    virusActivo.Activo = false;
                    db.Entry(virusActivo).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            return View(virus);
        }

        // GET: Virus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Virus virus = db.Virus.Find(id);
            if (virus == null)
            {
                return HttpNotFound();
            }
            return View(virus);
        }

        // POST: Virus/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdVirus,Activo,Patron")] Virus virus)
        {
            virus.IdPersona = Acceso.loguedUser.IdPersona;
            virus.Fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(virus).State = EntityState.Modified;
                db.SaveChanges();

                Virus virusActivo = this.db.Virus.Where(r => r.Activo == true && r.IdVirus != virus.IdVirus).FirstOrDefault();

                if (virusActivo !=null)
                {
                    virusActivo.Activo = false;
                    db.Entry(virusActivo).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            return View(virus);
        }

        // GET: Virus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Virus virus = db.Virus.Find(id);
            if (virus == null)
            {
                return HttpNotFound();
            }
            return View(virus);
        }

        // POST: Virus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Virus virus = db.Virus.Find(id);
            db.Virus.Remove(virus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
