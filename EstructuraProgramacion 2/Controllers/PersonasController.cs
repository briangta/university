﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;
using EstructuraProgramacion_2.Models.Enumeradores;

namespace EstructuraProgramacion_2.Controllers
{
    public class PersonasController : Controller
    {
        private EstructuraProgramacion2Entities db = new EstructuraProgramacion2Entities();

        // GET: Personas/Create
        public ActionResult Login()
        {
            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "usuario,clave")] Persona persona)
        {
            if (!string.IsNullOrWhiteSpace(persona.usuario)&&!string.IsNullOrWhiteSpace(persona.clave))
            {
                Persona loguedUser=this.db.Persona
                    .Where(r => r.usuario.Equals(persona.usuario) && r.clave.Equals(persona.clave))
                    .FirstOrDefault();

                if (loguedUser == null)
                {
                    ModelState.AddModelError("usuario", "Usuario o clave invalidos");
                    return this.View(persona);
                }

                Rol loguedUserRol = this.db.Rol
                    .Where(r => r.IdRol == loguedUser.IdRol)
                    .FirstOrDefault();

                Acceso.loguedUser = loguedUser;
                Acceso.rolLoguedUser = loguedUserRol;

                if (Acceso.AlloAccess(EnumRol.Admin))
                    return RedirectToAction("Index", "Virus", null);
                return RedirectToAction("Index", "Patrones", null);
            }

            ModelState.AddModelError("usuario", "Usuario o clave invalidos");
            return View(persona);
        }
        
        [Acceso(EnumRol.Admin)]
        public ActionResult Index()
        {
            if (!Acceso.AlloAccess(EnumRol.Admin))
                return RedirectToAction("Login", "Personas", null);

            var persona = db.Persona.Include(p => p.Rol);
            return View(persona.ToList());
        }

        // GET: Personas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "Nombre");
            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPersona,Nombre,Identificacion,Direccion,IdRol,usuario,clave")] Persona persona)
        {
            if (this.db.Persona.Where(r => r.Identificacion == persona.Identificacion).Count() > 0)
                ModelState.AddModelError("Identificacion", "Este numero de identificacion ya esta en uso");

            if (this.db.Persona.Where(r => r.usuario == persona.usuario).Count() > 0)
                ModelState.AddModelError("usuario", "Este nombre de usuario ya esta en uso");

            if (ModelState.IsValid)
            {
                db.Persona.Add(persona);
                db.SaveChanges();

                if (!Acceso.AlloAccess(EnumRol.Admin))
                    return RedirectToAction("Login", "Personas", null);
                   
                return RedirectToAction("Index");
            }

            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "Nombre", persona.IdRol);
            return View(persona);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!Acceso.AlloAccess(EnumRol.Admin))
                return RedirectToAction("Login", "Personas", null);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "Nombre", persona.IdRol);
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPersona,Nombre,Identificacion,Direccion,IdRol,usuario,clave")] Persona persona)
        {
            if (this.db.Persona.Where(r => r.Identificacion == persona.Identificacion && r.IdPersona!=persona.IdPersona).Count() > 0)
                ModelState.AddModelError("Identificacion", "Este numero de identificacion ya esta en uso");

            if (this.db.Persona.Where(r => r.usuario == persona.usuario && r.IdPersona != persona.IdPersona).Count() > 0)
                ModelState.AddModelError("usuario", "Este nombre de usuario ya esta en uso");

            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "Nombre", persona.IdRol);
            return View(persona);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
