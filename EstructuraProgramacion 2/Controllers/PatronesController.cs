﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;
using EstructuraProgramacion_2.Models.Enumeradores;

namespace EstructuraProgramacion_2.Controllers
{
    public class PatronesController : Controller
    {
        private EstructuraProgramacion2Entities db = new EstructuraProgramacion2Entities();

        // GET: Patrones
        public ActionResult Index()
        {
            var patrones = db.Patrones.Include(p => p.Persona);

            if (Acceso.AlloAccess(EnumRol.User))
                patrones = patrones.Where(r => r.IdPersona == Acceso.loguedUser.IdPersona);

            return View(patrones.ToList());
        }

        // GET: Patrones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patrones patrones = db.Patrones.Find(id);
            if (patrones == null)
            {
                return HttpNotFound();
            }
            return View(patrones);
        }

        // GET: Patrones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Patrones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPatron,Patron")] Patrones patrones)
        {
            patrones.IdPersona = Acceso.loguedUser.IdPersona;
            patrones.Fecha = DateTime.Now;

            if (db.Patrones.Where(r => r.IdPersona == patrones.IdPersona).Count() > 2)
                ModelState.AddModelError("Patron", "Este usuario ya tiene los 3 intentos utilizados");

            if (ModelState.IsValid)
            {
                db.Patrones.Add(patrones);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(patrones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
