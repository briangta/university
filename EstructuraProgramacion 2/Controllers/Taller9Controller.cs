using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;

namespace EstructuraProgramacion_2.Controllers
{
    public class Taller9Controller : Controller
    {
        // GET: Taller7
        public ActionResult Index(int? nivel, double? a, double? b, double? c, double? d)
        {
            if (nivel != null)
            {
                ViewBag.nivel = nivel.Value;
                if (a != null)
                    ViewBag.a = a.Value;
                ViewBag.b = b.Value;
                ViewBag.c = c.Value;
                ViewBag.d = d.Value;
            }

            return View();
        }

        public ActionResult asp(int? nivel, double? a, double? b, double? c, double? d)
        {
            if (nivel != null)
            {
                string resultado = string.Empty;
                int n;
                n = nivel.Value;
                b *= 2;
                c *= 1;
                d *= 0; /*este si no 
            /sirve para nada*/

                // Nivel 3 ->
                if (a != null && n == 3)
                {
                    a *= 3;
                    resultado += a + "x^2+";
                }

                resultado += b + "x+" + c;
                ViewBag.Resultado = resultado;
            }

            return View();
        }
    }
}