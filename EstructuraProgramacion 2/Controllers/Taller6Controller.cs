using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EstructuraProgramacion_2.Controllers
{
    public class Taller6Controller : Controller
    {
        // GET: Taller6
        public ActionResult Index(string texto)
        {
            if (!string.IsNullOrWhiteSpace(texto))
            {
                int contador = 0;
                string textoNew = string.Empty;
                texto.Trim();
                string[] palabras = texto.Split(' ');

                foreach (string a in palabras)
                {
                    textoNew = new String(a.Where(Char.IsLetterOrDigit).ToArray());
                    if (!string.IsNullOrWhiteSpace(textoNew))
                        contador++;
                }
                ViewBag.CantidadPalabras = contador;
            }

            return View();
        }

        public ActionResult Punto2(string texto, bool? mostrar)
        {
            if (HttpContext.Session["personas"] == null)
            {
                HttpContext.Session["personas"] = new List<string>();
            }

            List<string> personas = (List<string>)HttpContext.Session["personas"];
            if (!string.IsNullOrWhiteSpace(texto))
            {
                personas.Add(texto);
            }

            if(mostrar!=null && mostrar.Value)
            {
                ViewBag.Nombres = personas;
            }
            else
            {
                ViewBag.Nombres = null;
            }

            return View();
        }
    }
}