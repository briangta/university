﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EstructuraProgramacion_2.Controllers
{
    public class Taller11Controller : Controller
    {
        // GET: Taller11
        public ActionResult Index()
        {
            return View();
        }

        public string letrasIguales(string primero,string segundo)
        {
            string caracteresRepetidos = string.Empty;
            char[] letras1 = primero.ToCharArray();
            char[] letras2 = segundo.ToCharArray();

            foreach (char letra in letras1) {
                if (letras2.Contains(letra))
                {
                    caracteresRepetidos+=letra;
                }
            }

            return caracteresRepetidos;
        }
    }
}