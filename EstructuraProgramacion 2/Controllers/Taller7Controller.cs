﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;

namespace EstructuraProgramacion_2.Controllers
{
    public class Taller7Controller : Controller
    {
        // GET: Taller7
        public ActionResult Index()
        {
            if (System.Web.HttpContext.Current.Session["FirstLogin"] == null) { 
                ViewBag.Mensaje = "Hola mundo";
                System.Web.HttpContext.Current.Session["FirstLogin"] = 1;
            }
            else
            {
                ViewBag.Mensaje = "hola otra vez";
            }

            if (System.Web.HttpContext.Current.Session["persona"] == null)
            return View();

            Persona persona = (Persona)System.Web.HttpContext.Current.Session["persona"];
            return View(persona);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Name,LlastName,BornDate,Email")] Persona persona)
        {
            if (System.Web.HttpContext.Current.Session["FirstLogin"] == null)
            {
                ViewBag.Mensaje = "Hola mundo";
                System.Web.HttpContext.Current.Session["FirstLogin"] = 1;
            }
            else
            {
                ViewBag.Mensaje = "hola otra vez";
            }

            if (ModelState.IsValid)
            {
                ViewBag.creado = true;
                System.Web.HttpContext.Current.Session["persona"] = persona;
                return RedirectToAction("Index");
            }
                
            return View(persona);
        }
    }
}