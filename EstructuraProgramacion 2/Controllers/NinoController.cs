﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstructuraProgramacion_2.Models;

namespace EstructuraProgramacion_2.Controllers
{
    public class NinoController : Controller
    {
        private EstructuraProgramacion2Entities db = new EstructuraProgramacion2Entities();

        // GET: Nino
        public ActionResult Index(bool? soloNinoConJuguetes)
        {
            MarcaJuguete JugueteMasRepetido = db.NinoJuguete
                .GroupBy(r => r.MarcaJuguete)
                .OrderByDescending(r => r.Count())
                .Select(r=>r.Key)
                .FirstOrDefault();

            if (JugueteMasRepetido != null)
                ViewBag.JugueteMasRepetido = JugueteMasRepetido.Nombre;

            IQueryable<Nino> nino = db.Nino;
            if (soloNinoConJuguetes != null && soloNinoConJuguetes.Value)
                nino = nino.Where(r => r.NinoJuguete.Count() > 0);
            return View(nino.ToList());
        }

        // GET: Nino/Create
        public ActionResult Create()
        {
            ViewBag.ListaJuguetes = db.MarcaJuguete.ToList();
            return View();
        }

        // POST: Nino/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Edad,Nombre")] Nino nino, string idJuguete)
        {
            if (ModelState.IsValid)
            {
                db.Nino.Add(nino);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(idJuguete))
                {
                    List<NinoJuguete> ninoJuguetes = new List<NinoJuguete>();

                    foreach (string idJug in idJuguete.Split(','))
                    {
                        NinoJuguete ninoJuguete = new NinoJuguete();
                        ninoJuguete.IdNino = nino.Id;
                        ninoJuguete.IdJuguete = int.Parse(idJug);
                        ninoJuguetes.Add(ninoJuguete);
                    }

                    db.NinoJuguete.AddRange(ninoJuguetes);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.ListaJuguetes = db.MarcaJuguete.ToList();
            return View(nino);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
