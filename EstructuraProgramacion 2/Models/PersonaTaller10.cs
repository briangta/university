﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EstructuraProgramacion_2.Models
{
    public class PersonaTaller10
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Id { get; set; }
    }
}