//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template made by Louis-Guillaume Morand.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;

namespace EstructuraProgramacion_2.Models
{
    
    
    public class NinoMetaData
    {
    	[Key]
    	//[NombreMostrar("Nino_Id","Id")]
        [Required]
        public virtual int Id
        {
            get;
            set;
        }
    	
        [StringLength(50, ErrorMessage="El campo no puede ser mayor a 50 caracteres")]
    	//[NombreMostrar("Nino_Edad","Edad")]
        public virtual string Edad
        {
            get;
            set;
        }
    	
        [StringLength(50, ErrorMessage="El campo no puede ser mayor a 50 caracteres")]
    	//[NombreMostrar("Nino_Nombre","Nombre")]
        public virtual string Nombre
        {
            get;
            set;
        }
    }
}
