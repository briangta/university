//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template made by Louis-Guillaume Morand.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;

namespace EstructuraProgramacion_2.Models
{
    
    
    public class NinoJugueteMetaData
    {
    	[Key]
    	//[NombreMostrar("NinoJuguete_Id","Id")]
        [Required]
        public virtual int Id
        {
            get;
            set;
        }
    	//[NombreMostrar("NinoJuguete_IdNino","IdNino")]
        public virtual Nullable<int> IdNino
        {
            get;
            set;
    
        }
    	//[NombreMostrar("NinoJuguete_IdJuguete","IdJuguete")]
        public virtual Nullable<int> IdJuguete
        {
            get;
            set;
    
        }
    }
}
