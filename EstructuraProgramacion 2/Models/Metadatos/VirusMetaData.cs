//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template made by Louis-Guillaume Morand.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;

namespace EstructuraProgramacion_2.Models
{
    
    
    public class VirusMetaData
    {
    	[Key]
    	//[NombreMostrar("Virus_IdVirus","IdVirus")]
        [Required]
        public virtual int IdVirus
        {
            get;
            set;
        }
    	
        [StringLength(200, ErrorMessage="El campo no puede ser mayor a 200 caracteres")]
    	//[NombreMostrar("Virus_Patron","Patron")]
        [Required]
        public virtual string Patron
        {
            get;
            set;
        }
    	//[NombreMostrar("Virus_Activo","Activo")]
        [Required]
        public virtual bool Activo
        {
            get;
            set;
        }
    	//[NombreMostrar("Virus_Fecha","Fecha")]
        [Required]
        public virtual System.DateTime Fecha
        {
            get;
            set;
        }
    	//[NombreMostrar("Virus_IdPersona","IdPersona")]
        [Required]
        public virtual int IdPersona
        {
            get;
            set;
    
        }
    }
}
