﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraProgramacion_2.Models
{
    public partial class Patrones
    {
        public int? VirusEliminados { get; set; }
        public bool? VirusTotalmenteEliminado { get; set; }
    }
}