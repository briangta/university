﻿using EstructuraProgramacion_2.Models.Enumeradores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EstructuraProgramacion_2.Models
{
    public class Acceso : ActionFilterAttribute
    {

        public Acceso(EnumRol rol)
        {
            acceso = AlloAccess(rol);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (!acceso)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Personas",
                    action = "Login"
                }));
            }
        }

        public static bool AlloAccess(EnumRol rol)
        {
            if (HttpContext.Current.Session["rolLoguedUser"] != null && ((Rol)HttpContext.Current.Session["rolLoguedUser"]).IdRol == (int)rol)
            {
                acceso = true;
                return true;
            }

            acceso = false;
            return false;
        }

        public static Persona loguedUser
        {
            get
            {
                return (Persona)HttpContext.Current.Session["loguedUser"];
            }
            set
            {
                HttpContext.Current.Session["loguedUser"] = value;
            }
        }

        public static bool acceso
        {
            get
            {
                return (bool)HttpContext.Current.Session["acceso"];
            }
            set
            {
                HttpContext.Current.Session["acceso"] = value;
            }
        }

        public static Rol rolLoguedUser
        {
            get
            {
                return (Rol)HttpContext.Current.Session["rolLoguedUser"];
            }
            set
            {
                HttpContext.Current.Session["rolLoguedUser"] = value;
            }
        }


    }

}